# Configurando ambiente django

## Diretório do Projeto

Crie um diretório para o projeto e 3 arquivos dentro dele com os seguintes nomes:

- Dockerfile
- docker-compose.yml
- requirements.txt

 Segue o conteúdo dos arquivos:

 ## docker-compose.yml

```
version: '3'

services:
  db:
    image: postgres
  web:
    build: .
    command: python manage.py runserver 0.0.0.0:8000
    volumes:
      - .:/code
    ports:
      - "8000:8000"
    depends_on:
      - db
```

## Dockerfile

```
FROM python:3
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
COPY requirements.txt /code/
RUN pip install -r requirements.txt
COPY . /code/

```

## Requirements.txt

```
Django==2.2.5
psycopg2-binary==2.8.3
```

## Execute o seguinte comando

```
$ docker-compose up
```

Agora o servidor esta rodando, caso queira desligar o servidor, basta pressionar "ctrl + c" ou digite o comando:

```
$ docker-compose down
```
Se quiser startar o projeto novamente, basta executar docker-compose up, novamente.

## Comandos

Para executar qualquer comando você precisa fazer da seguinte forma

```
$ docker-compose exec web [comando que deseja executar]
```
exemplo:

```
$ docker-compose exec web python manage.py migrate
```

## Importante: 

Lembre-se de executar as migrations para o projeto funcionar.

Há uma url isolada "localhost:8000/produtos/" que serve para cadastro de produtos, caso não queira utilizar o admin.