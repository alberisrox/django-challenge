from django.urls import path
from . import views



urlpatterns = [
    path('', views.homeCliente),
    path('home/', views.homeCliente),
    
    path('produtos/', views.produtoList),
    path('produtos/<int:id>', views.produtoView),
    path('novoProduto/', views.novoProduto),
    path('editarProduto/<int:id>', views.editarProduto),
    path('deletarProduto/<int:id>', views.deletarProduto),
    
    path('pedidos/', views.pedidoList),
    path('meuPedido/', views.pedidoView),
    path('cancelarPedido/<int:id>', views.cancelarPedido),
    path('concluirPedido/<int:id>', views.concluirPedido),
    
    path('adicionarItem/<int:id>', views.adicionarItem),
    path('removerItem/<int:id>', views.removerItem),
    
    path('adicionais/', views.adicionalList),
    path('adicionais/<int:id>', views.adicionalView),
   
]
