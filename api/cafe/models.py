from django.db import models
from django.contrib.auth import get_user_model

# region produtos

class produto(models.Model):   
    de_nome = models.CharField(max_length = 50)
    nu_preco = models.DecimalField(max_digits=5, decimal_places = 2)
    flag_adicional = models.BooleanField(default = False)

    def __str__(self):
        return self.de_nome
    

class adicional(models.Model):        
    de_nome = models.CharField(max_length = 50)
    nu_preco = models.DecimalField(max_digits=5, decimal_places = 2, default = 0)
    
    def __str__(self):
        return self.de_nome
6    
# endregion

# region pedidos
       
class pedido(models.Model): 
    ESTATUS = {
        ('PREPARANDO', 'PREPARANDO'),
        ('PRONT', 'PRONTO'),
        ('PAGO', 'PAGO'),
        ('ENTREGUE', 'ENTREGUE')
    }
    
    flag_delivery = models.BooleanField(default = False) 
    de_estatus = models.CharField(max_length = 10, choices = ESTATUS, default = 'PREPARANDO')
    nu_cliente = models.ForeignKey(get_user_model(), verbose_name = "cliente", on_delete=models.CASCADE)
    flag_aberto = models.BooleanField(default = True)

    
    def __str__(self):
        return str(self.id)

class pedido_produto(models.Model):   
    TAMANHO ={
        ('PEQUENO', 'PEQUENO'),
        ('MEDIO', 'MEDIO'),
        ('GRANDE', 'GRANDE')
    }

    TIPO ={
        ('SIMPLES', 'SIMPLES'),
        ('DUPLO', 'DUPLO'),
        ('TRIPLO', 'TRIPLO'),
    }
    
    nu_pedido = models.ForeignKey("pedido", on_delete = models.CASCADE)
    nu_produto = models.ForeignKey("produto", on_delete = models.CASCADE)
    nu_adicional = models.ForeignKey("adicional", verbose_name = "adicional" , on_delete = models.CASCADE, blank = True, null= True)
    de_tipo = models.CharField(max_length=7, verbose_name = "tipo", choices = TIPO)
    de_tamanho = models.CharField(max_length=7, verbose_name = "tamanho", choices = TAMANHO)
    flag_pronto = models.BooleanField(default = False)

    def __str__(self):
        if self.nu_adicional != None:
            return self.nu_produto.de_nome + " com " + self.nu_adicional.de_nome
        else:
            return self.nu_produto.de_nome

# endregion