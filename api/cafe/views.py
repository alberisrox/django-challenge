from django.shortcuts import render, get_object_or_404, redirect
from .models import *
from django.http import HttpResponse
from .forms import *
from django.contrib.auth.decorators import login_required

# region home
def homeCliente(request):
    produtos = produto.objects.all().order_by('de_nome')
    return render(request, 'cafe/homeCliente.htm', {'produtos' : produtos}) 
# endregion

# region produtos

@login_required
def produtoList(request):
    produtos = produto.objects.all().order_by('de_nome')
    return render(request, 'cafe/list.htm', {'produtos' : produtos})

def produtoView(request, id):
    produt = get_object_or_404(produto, pk = id)
    return render(request, 'cafe/produto.htm', {'produto' : produt})

@login_required
def novoProduto(request):
    if request.method == 'POST':
        form = ProdutoForm(request.POST)
        
        if form.is_valid():
            produt = form.save(commit = False)
            produt.save()
            return redirect('/produtos/')
        else:
            return render(request, 'cafe/editarProduto.htm', {'form' : form})
    else:
        form = ProdutoForm()
        return render(request, 'cafe/novoProduto.htm', {'form' : form})      

@login_required  
def editarProduto(request, id):
    produt = get_object_or_404(produto, pk = id)
    form = ProdutoForm(instance=produt)
    if request.method == 'POST':
        form = ProdutoForm(request.POST, instance = produt)
        if form.is_valid():
            produt.save()
            return redirect('/produtos/')
        else:
            return render(request, 'cafe/editarProduto.htm', {'form' : form,'produto' : produt})
    else:
        return render(request, 'cafe/editarProduto.htm', {'produto' : produt, 'form' : form})

@login_required   
def deletarProduto(request, id):
    produt = get_object_or_404(produto, pk = id)
    produt.delete()
    return redirect('/produtos/')
# endregion

# region pedido
@login_required
def pedidoList(request):
    pedidos = pedido.objects.all()
    return render(request, 'cafe/pedidosList.htm', {'pedidos' : pedidos})

@login_required
def pedidoView(request):
    if not pedido.objects.filter(flag_aberto = True, nu_cliente = request.user):  
        pedidos = adicionarPedido(request)
    else:
        pedidos = pedido.objects.get(flag_aberto = True, nu_cliente = request.user)
    produtos = pedidos.pedido_produto_set.all()
    preco = 0
    for pp in produtos:
        preco += pp.nu_produto.nu_preco
        if pp.nu_adicional != None:
            preco += pp.nu_adicional.nu_preco 
    return render(request, 'cafe/pedido.htm', {'produtos' : produtos, 'pedido' : pedidos, 'preco' : preco})

@login_required
def adicionarPedido(request):
    p =pedido(nu_cliente = request.user)
    p.save()
    return p 

@login_required
def cancelarPedido(request, id):
    pedid = get_object_or_404(pedido, pk = id)
    pedid.delete()
    return redirect('/home/')

def concluirPedido(request, id):
    pedid = get_object_or_404(pedido, pk = id)
    form = PedidoForm(instance=pedid)
    if request.method == 'POST':
        form = PedidoForm(request.POST, instance = pedid)
        if form.is_valid():
            p = form.save(commit = False)
            p.flag_aberto = False
            p.save()
            return redirect('/produtos/')
        else:
            return render(request, 'cafe/pedido.htm', {'form' : form})
    else:
        return render(request, 'cafe/concluirPedido.htm', {'form' : form})

# endregion

# region adicional

@login_required
def adicionalList(request):
    adicionais = adicional.objects.all()
    return render(request, 'cafe/adicionaisList.htm', {'adicionais' : adicionais})

@login_required
def adicionalView(request, id):
    adicionais = get_object_or_404(adicional, pk = id)
    return render(request, 'cafe/adicional.htm', {'adicional' : adicionais})
# endregion

#region itens
@login_required
def adicionarItem(request, id):
    produt = get_object_or_404(produto, pk = id)
    pedid = pedido.objects.filter(nu_cliente = request.user, flag_aberto = True)
    p = pedido()
    if not pedid:
        p = adicionarPedido(request)
    else:
        p = pedido.objects.get(nu_cliente = request.user, flag_aberto = True)        
    if request.method == 'POST':
        if produt.flag_adicional: 
            form = PedidoProdutoFormAdicional(request.POST)
        else:
            form = PedidoProdutoForm(request.POST)
        if form.is_valid():
            pp =form.save(commit = False)
            pp.nu_produto = produto.objects.get(pk = id)
            pp.nu_pedido = p
            pp.save()
            return redirect('/home/')
        else:
                if produt.flag_adicional: 
                    form = PedidoProdutoFormAdicional()  
                    return render(request, 'cafe/adicionarItem.htm', {'form' : form, 'produto' : produt})
                else:
                    form = PedidoProdutoForm()  
                    return render(request, 'cafe/adicionarItem.htm', {'form' : form, 'produto': produt})
    else:
        if produt.flag_adicional: 
            form = PedidoProdutoFormAdicional()  
            return render(request, 'cafe/adicionarItem.htm', {'form' : form, 'produto': produt})
        else:
            form = PedidoProdutoForm()  
            return render(request, 'cafe/adicionarItem.htm', {'form' : form, 'produto': produt})

@login_required
def removerItem(request, id):
    item = get_object_or_404(pedido_produto, pk = id)
    item.delete()
    return redirect('/meuPedido/')

# endregion
