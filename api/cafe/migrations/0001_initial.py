# Generated by Django 2.2.5 on 2019-09-16 15:42

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='adicional',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('de_nome', models.CharField(max_length=50)),
                ('nu_preco', models.DecimalField(decimal_places=2, default=0, max_digits=5)),
            ],
        ),
        migrations.CreateModel(
            name='produto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('de_nome', models.CharField(max_length=50)),
                ('nu_preco', models.DecimalField(decimal_places=2, max_digits=5)),
                ('de_tipo', models.CharField(choices=[('T', 'TRIPLO'), ('S', 'SIMPLES'), ('D', 'DUPLO')], max_length=1)),
                ('de_tamanho', models.CharField(choices=[('P', 'PEQUENO'), ('G', 'GRANDE'), ('M', 'MEDIO')], max_length=1)),
                ('nu_adicionais', models.ManyToManyField(blank=True, to='cafe.adicional')),
            ],
        ),
        migrations.CreateModel(
            name='pedido',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('de_entregar', models.BooleanField(default=False)),
                ('de_estatus', models.CharField(choices=[('AP', 'A PAGAR'), ('PT', 'PRONTO'), ('PP', 'PREPARANDO'), ('PG', 'PAGO'), ('EN', 'ENTREGUE')], max_length=2)),
                ('nu_produto', models.ManyToManyField(to='cafe.produto')),
            ],
        ),
    ]
