# Generated by Django 2.2.5 on 2019-09-17 22:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cafe', '0012_auto_20190917_2112'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pedido',
            name='de_estatus',
            field=models.CharField(choices=[('PRONT', 'PRONTO'), ('PAGO', 'PAGO'), ('ENTREGUE', 'ENTREGUE'), ('PREPARANDO', 'PREPARANDO')], default='PREPARANDO', max_length=10),
        ),
        migrations.AlterField(
            model_name='pedido_produto',
            name='de_tamanho',
            field=models.CharField(choices=[('MEDIO', 'MEDIO'), ('GRANDE', 'GRANDE'), ('PEQUENO', 'PEQUENO')], max_length=7, verbose_name='tamanho'),
        ),
    ]
