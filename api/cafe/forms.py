from django import forms
from .models import *

class ProdutoForm(forms.ModelForm):
    
    class Meta:
        model = produto
        fields = ("de_nome", "nu_preco", "flag_adicional")
    
class PedidoProdutoFormAdicional(forms.ModelForm):
    
    class Meta:
        model = pedido_produto
        fields = ("nu_adicional", "de_tipo", "de_tamanho")
        
class PedidoProdutoForm(forms.ModelForm):
    
    class Meta:
        model = pedido_produto
        fields = ("de_tamanho", "de_tipo")

class PedidoForm(forms.ModelForm):
    
    class Meta:
        model = pedido
        fields = ("flag_delivery",)

