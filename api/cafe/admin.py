from django.contrib import admin
from .models import *

admin.site.register(produto)
admin.site.register(adicional)
admin.site.register(pedido)
admin.site.register(pedido_produto)
